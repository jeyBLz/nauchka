import os

import skimage.io
from skimage.io import imread
from skimage.util import crop

path = 'C:\\Users\\andre\\OneDrive\\Рабочий стол\\Научка\\sources'

destination_path = 'C:\\Users\\andre\\OneDrive\\Рабочий стол\\Научка\\sources'

if not os.path.isdir(destination_path):
    os.mkdir(destination_path)

for curr_dir in os.listdir(path):
    curr_dir_path = path + '\\{}'.format(curr_dir)
    destination_dir_path = destination_path + '\\{}'.format(curr_dir)
    if not os.path.isdir(destination_dir_path):
        os.mkdir(destination_dir_path)
    for curr_file in os.listdir(curr_dir_path):
        curr_file_path = curr_dir_path + '\\{}'.format(curr_file)
        destination_dir_file_path = destination_dir_path + '\\{}'.format(curr_file)
        img = imread(curr_file_path)
        img = crop(img, ((140, 140), (230, 210), (0, 0)), copy=False)  # (44, 44), (125, 107), (0, 0)
        skimage.io.imsave(destination_dir_file_path, img)
