from tensorflow.python.keras.models import load_model
import numpy as np
from skimage.util import crop
from PIL import Image


def get_model_answer(file_entry):
    loaded_model = load_model('model-new-test.h5')

    image_file_path = file_entry.get()
    img = Image.open(image_file_path).convert('RGB').resize((200, 200), Image.ANTIALIAS)

    width, height = img.size

    # img = crop(img, ((abs((height - 200) // 2), abs((height - 200) // 2)), (abs((width - 200) // 2), abs((width - 200) // 2)), (0, 0)), copy=False)

    labels = ['0.0', '0.5', '1.0', '1.5', '2.0', '2.5', '3.0', '3.5', '4.0', '4.5', '5.0']

    prediction = loaded_model.predict(img[None, :, :])
    classes = np.argmax(prediction, axis=1)

    return labels[classes[0]]
