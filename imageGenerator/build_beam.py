import numpy as np
import matplotlib.pyplot as plt
import time
from scipy.special import jv


def f(r_arr, radius, m):
    return np.array([np.exp(-1 * (r ** 2 / radius ** 2)) * r ** np.abs(m) for r in r_arr])


def restored_function(function, N, m):
    n = N - 1
    a = np.zeros((2 * N + 1, 2 * N + 1), dtype='complex')
    for j in range(2 * N + 1):
        for k in range(2 * N + 1):
            alpha = int(np.sqrt((j - n) ** 2 + (k - n) ** 2))
            if alpha > n:
                continue
            else:
                a[j][k] = function[alpha] * np.exp(complex(0, 1) * m * np.arctan2(k - n, j - n))
    return a


def hankel_transformation(function, array_points, n, m):
    x = array_points
    h_r = x[1] - x[0]
    integral = np.zeros(n, dtype=np.complex128)
    for x_item, i in zip(x, range(len(x))):
        integral[i] = np.sum(function * jv(m, 2 * np.pi * x * x_item) * x * h_r)
    return integral * (2 * np.pi / 1j ** m)


def get_amplitude_and_phase_graphs_2d(function, R, amplitude_title, phase_title):
    fig, arr = plt.subplots(1, 2, figsize=(10, 5))
    arr[0].imshow(np.abs(function), extent=[-R, R, -R, R], cmap='hot', interpolation='nearest')
    arr[0].set_title(amplitude_title)
    phase = arr[1].imshow(np.angle(function), extent=[-R, R, -R, R], cmap='hot', interpolation='nearest')
    arr[1].set_title(phase_title)
    fig.colorbar(phase, ax=arr[1])
    plt.show()


# Задаём исходные значения
R = 2  # радиус
const_m_1 = 2  # порядок пучка
const_m_2 = 0
n = 2 ** 8  # количество точек разбиения
N = 2 * n + 1
r_disc = np.linspace(0, R, n, endpoint=False)

h_r = R / n

func_1 = f(r_disc, R, const_m_1)
func_2 = f(r_disc, R, const_m_2)

func = func_2

hankel_transformation_result_1 = hankel_transformation(func_1, r_disc, n, const_m_1)
restored_hankel_transformation_result_1 = restored_function(hankel_transformation_result_1, n, const_m_1)
hankel_transformation_result_2 = hankel_transformation(func_2, r_disc, n, const_m_2)
restored_hankel_transformation_result_2 = restored_function(hankel_transformation_result_2, n, const_m_2)
restored_hankel_transformation_result = restored_hankel_transformation_result_1 + restored_hankel_transformation_result_2
plt.axis('off')
plt.imshow(np.abs(restored_hankel_transformation_result), cmap='hot', interpolation='nearest')
plt.show()
