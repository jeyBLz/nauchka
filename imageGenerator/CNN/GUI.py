from tkinter import *
from tkinter.filedialog import askopenfilename
from tkinter import messagebox as mb
from PIL import Image, ImageTk
from program import get_model_answer
import sys

window = Tk()
window.title('Нейронная сеть')
window.resizable(False, False)


def on_closing():
    if mb.askokcancel("Выход из приложения", "Хотите выйти?"):
        sys.exit(0)


def get_answer():
    if file_entry.get() == 'Выберите файл':
        mb.showinfo("Ошибка!", "Необходимо выбрать файл пучка")
        return
    answer_entry.config(state='normal')
    answer_entry.delete(0, 'end')
    answer_entry.insert(0, get_model_answer(file_entry))
    answer_entry.config(state='readonly')


def browse():
    filetypes = (('png file', '*.png'),
                 ('jpg file', '*.jpg'),
                 ('jpeg file', '*.jpeg'),
                 ("All files", "*.*"))
    temp_filename = file_entry.get()
    filename = askopenfilename(filetypes=filetypes)
    if filename == '':
        filename = temp_filename
    file_entry.config(state='normal')
    file_entry.delete(0, 'end')
    file_entry.insert(0, filename)
    file_entry.config(state='readonly')
    global image, photo
    if filename != 'Выберите файл':
        image = Image.open(file_entry.get()).resize((200, 200), Image.ANTIALIAS)
        photo = ImageTk.PhotoImage(image)
        image = image_canvas.create_image(0, 0, anchor='nw', image=photo)


window.protocol("WM_DELETE_WINDOW", on_closing)

file_entry = Entry(window, font=40)
file_entry.insert(0, 'Выберите файл')
file_entry.grid(row=1, column=1, ipadx=5, ipady=5, padx=10, pady=10)
file_entry.config(state='readonly')

browse_button = Button(window, text='НАЙТИ', font=40, command=browse)
browse_button.grid(row=1, column=2, padx=10, pady=10)

answer_entry = Entry(window, font=40)
answer_entry.insert(0, 'Здесь будет ответ')
answer_entry.grid(row=2, column=1, ipadx=5, ipady=5, padx=10, pady=10)
answer_entry.config(state='readonly')

answer_button = Button(window, text='ОТВЕТ', font=40, command=get_answer)
answer_button.grid(row=2, column=2, padx=10, pady=10)

image_canvas = Canvas(window, height=200, width=200)
image_canvas.grid(row=3, column=1, rowspan=2, columnspan=2)
image = Image.open('C:\\Users\\andre\\OneDrive\\Рабочий стол\\Научка\\Исходники\\source(0.0)copy.png').resize((200, 200), Image.ANTIALIAS)
photo = ImageTk.PhotoImage(image)
image = image_canvas.create_image(0, 0, anchor='nw', image=photo)


window.mainloop()
