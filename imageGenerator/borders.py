import os
import PIL
from PIL import Image

path = 'C:\\Users\\andre\\OneDrive\\Рабочий стол\\Научка\\sources\\5.0\\sources00-1(5.0).png'


img = Image.open(path)
rgb_img = img.convert('RGB')

necessary_pixels = [(255, 255, 255),
                    (10, 0, 0)]
prev = necessary_pixels[0]
for width in range(rgb_img.size[0]):
    if prev == necessary_pixels[0] and rgb_img.getpixel((width, 215)) == necessary_pixels[1]:
        print(width, end=' ')
    if prev == necessary_pixels[1] and rgb_img.getpixel((width, 215)) == necessary_pixels[0]:
        print(width, end=' ')
    prev = rgb_img.getpixel((width, 215))

print()

for height in range(rgb_img.size[1]):
    if prev == necessary_pixels[0] and rgb_img.getpixel((144, height)) == necessary_pixels[1]:
        print(height, end=' ')
    if prev == necessary_pixels[1] and rgb_img.getpixel((144, height)) == necessary_pixels[0]:
        print(height, end=' ')
    prev = rgb_img.getpixel((144, height))
