import numpy as np
import matplotlib.pyplot as plt
# from google.colab import files
import os

matrices = []


def func(x, y, w0, m0):
    return np.exp(-(x ** 2 + y ** 2) / w0 ** 2) * (x + 1j * np.sign(m0) * y) ** np.abs(m0)


def matrix_archive(matrix, num, q_shape, shift):
    new_matrix = np.zeros(shape=(q_shape, q_shape))
    range_min = num // 2 - q_shape // 2
    range_max = num // 2 + q_shape // 2
    for ii in range(range_min, range_max, 1):
        for jj in range(range_min, range_max, 1):
            new_matrix[ii - range_min][jj - range_min] = matrix[ii][jj]
    return new_matrix


def cut_matrix(matrix, m, parameter):
    n = int(matrix.shape[0] * 0.75)
    path = 'C:\\Users\\andre\\OneDrive\\Рабочий стол\\Научка\\sources\\sources{}{}{}{}({}).png'
    for hh in range(0, matrix.shape[0] - n, 500):
        for vv in range(0, matrix.shape[1] - n, 500):
            if os.path.isfile(path.format(parameter * 10, hh, vv, int(np.sign(m)), np.abs(m))):
                continue
            new_matrix = matrix[0 + hh:n + hh, 0 + vv:n + vv]
            plt.axis('off')
            plt.imshow(new_matrix, cmap='hot', interpolation='nearest')
            plt.savefig(fname=path.format(parameter * 10, hh, vv, int(np.sign(m)), np.abs(m)), format='png', transparent=True)
            print(path.format(parameter * 10, hh, vv, int(np.sign(m)), np.abs(m)))


dots_num = 2 ** 12  # 2 ** 12
x_range = y_range = np.linspace(-100, 100, dots_num)
ax_x, ax_y = np.meshgrid(x_range, y_range)

for mm in range(-10, 11, 1):  # range(-10, 11, 1)
    print(mm)
    m = float(mm) / 2

    parameter = 0.5
    z = np.fft.fftshift(np.fft.fft2(func(ax_x, ax_y, parameter, m)))  # изначально было 0.75

    z = np.abs(z)  # получение изображений

    # plt.axis('off')
    # plt.imshow(z, cmap='hot', interpolation='nearest')
    # path = 'C:\\Users\\andre\\OneDrive\\Рабочий стол\\Научка\\sources\\source{}{}({}).png'.format(parameter, int(np.sign(m)), np.abs(m))
    # plt.savefig(fname=path, format='png', transparent=True)

    # cut_matrix(z, m, parameter)

    # z = np.angle(z)  # получение фаз

    # z = matrix_archive(z, dots_num, 500, 100)

    # z = shift_matrix(z, -200, -2000)

    plt.axis('off')
    plt.imshow(z, cmap='hot', interpolation='nearest')
    plt.show()
    # print(m)

    # if not os.path.exists("D:/Study/6 semester/Sci-Fi/images/{arg}".format(arg=m)):
    #     os.mkdir(path="D:/Study/6 semester/Sci-Fi/images/{arg}".format(arg=m))
    #
    # plt.savefig(fname="D:/Study/6 semester/Sci-Fi/images/{arg}/{arg}(1).png".format(arg=m),
    #             format='png', transparent=True)
    # plt.show()
