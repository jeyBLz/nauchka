from tensorflow.python.keras.preprocessing.image import ImageDataGenerator
from tensorflow.python.keras.models import Sequential
from tensorflow.python.keras.layers import Conv2D, MaxPooling2D, Activation, Dropout, Flatten, Dense
from tensorflow.keras.callbacks import ModelCheckpoint

# Preparations

train_dir = 'C:\\Users\\andre\\OneDrive\\Рабочий стол\\Научка\\training'
val_dir = 'C:\\Users\\andre\\OneDrive\\Рабочий стол\\Научка\\valid'

img_width, img_height = 200, 200

input_shape = (img_width, img_height, 3)

epochs = 30
batch_size = 22

nb_train_samples = 1742 * 11
nb_validation_samples = 194 * 11

# CNN

model = Sequential()

model.add(Conv2D(32, (3, 3), input_shape=input_shape))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Conv2D(32, (3, 3), input_shape=input_shape))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Conv2D(32, (3, 3), input_shape=input_shape))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Flatten())
model.add(Dense(64))
model.add(Activation('relu'))
model.add(Dropout(0.5))
model.add(Dense(11))
model.add(Activation('sigmoid'))

model.compile(loss='categorical_crossentropy',
              optimizer='adam',
              metrics=['accuracy'])

# Training

weights_file = 'C:\\Users\\andre\\OneDrive\\Рабочий стол\\Научка\\weights.h5'
# callback = ModelCheckpoint(weights_file,
#                            monitor='accuracy',
#                            mode='max',
#                            save_best_only=True)

datagen = ImageDataGenerator(rescale=1. / 255)

train_generator = datagen.flow_from_directory(
    train_dir,
    target_size=(img_width, img_height),
    batch_size=batch_size,
    class_mode='categorical'
)

val_generator = datagen.flow_from_directory(
    val_dir,
    target_size=(img_width, img_height),
    batch_size=batch_size,
    class_mode='categorical'
)

model.fit(
    train_generator,
    steps_per_epoch=nb_train_samples // batch_size,
    epochs=epochs,
    validation_data=val_generator,
    validation_steps=nb_validation_samples // batch_size
    # callbacks=[callback]
)

# CNN saving

model.save('model.h5')

# json_file = 'C:\\Users\\andre\\OneDrive\\Рабочий стол\\Научка\\model.json'
# model_json = model.to_json()
#
# with open(json_file, 'w') as f:
#     f.write(model_json)
