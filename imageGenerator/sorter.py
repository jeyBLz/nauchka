import os
import os.path
import shutil
import re

path = 'C:\\Users\\andre\\OneDrive\\Рабочий стол\\Научка\\sources'

destination_folder = path + '\\{}'

reg_exp_arr = [re.compile(r'\(0\.0\)\.png'),
               re.compile(r'\(0\.5\)\.png'),
               re.compile(r'\(1\.0\)\.png'),
               re.compile(r'\(1\.5\)\.png'),
               re.compile(r'\(2\.0\)\.png'),
               re.compile(r'\(2\.5\)\.png'),
               re.compile(r'\(3\.0\)\.png'),
               re.compile(r'\(3\.5\)\.png'),
               re.compile(r'\(4\.0\)\.png'),
               re.compile(r'\(4\.5\)\.png'),
               re.compile(r'\(5\.0\)\.png')]

switch_case = {
    0: '0.0',
    1: '0.5',
    2: '1.0',
    3: '1.5',
    4: '2.0',
    5: '2.5',
    6: '3.0',
    7: '3.5',
    8: '4.0',
    9: '4.5',
    10: '5.0'
}

files = os.listdir(path)

count = 0
for file in files:
    reg_exp_count = 0
    for reg_exp in reg_exp_arr:
        if reg_exp.search(file):
            current_destination = destination_folder.format(switch_case[reg_exp_count])
            if not os.path.isdir(current_destination):
                os.mkdir(current_destination)
            shutil.move(os.path.join(path, file), current_destination)
            break
        reg_exp_count += 1
