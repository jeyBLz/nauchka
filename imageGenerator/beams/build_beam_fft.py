import numpy as np
import matplotlib.pyplot as plt


def f(x, y, w0, m0):
    return np.exp(-(x ** 2 + y ** 2) / w0 ** 2) * (x + 1j * np.sign(m0) * y) ** np.abs(m0)


def get_beam(dots_entry, w0_entry, m_entry):
    w0 = float(w0_entry.get())
    m = float(m_entry.get())
    dots_num = int(dots_entry.get())

    x_range = y_range = np.linspace(-100, 100, dots_num)
    ax_x, ax_y = np.meshgrid(x_range, y_range)

    z = np.fft.fftshift(np.fft.fft2(f(ax_x, ax_y, w0, m)))

    return np.abs(z)

