import os
import PIL
from PIL import Image
from PIL import ImageFilter

path = 'C:\\Users\\andre\\OneDrive\\Рабочий стол\\Научка\\sources'

for curr_dir in os.listdir(path):
    curr_dir_path = path + '\\{}'.format(curr_dir)
    for curr_file in os.listdir(curr_dir_path):
        curr_file_path = curr_dir_path + '\\{}'.format(curr_file)
        img = Image.open(curr_file_path)
        new_img = img.filter(ImageFilter.BLUR)
        new_img.save(curr_dir_path + '\\{}{}'.format('blurred-', curr_file))
