from tkinter import *
from tkinter.filedialog import asksaveasfilename
import matplotlib.pyplot as plt
from build_beam_fft import get_beam
from PIL import Image

root = Tk()
root.title('Построение пучков')
root.resizable(False, False)


def get_image():
    z = get_beam(dots_entry, w0_entry, m_entry)
    test_1 = plt.imshow(z, cmap='hot', interpolation='nearest')
    plt.show()
    r = plt.gcf().canvas.get_renderer()
    test_2 = test_1.make_image(renderer=r, magnification=2.0)
    print(test_2[0].shape)
    test_3 = Image.fromarray(test_2[0])
    test_3.show()
    test_3.save('test.png')
    print(type(test_2[0]))


w0_label = Label(root, text='Радиус')
w0_label.grid(row=1, column=1, ipadx=2, ipady=2, padx=5, pady=5)
w0_entry = Entry(root)
w0_entry.grid(row=1, column=2, ipadx=2, ipady=2, padx=5, pady=5)
w0_entry.insert(0, '5')

m_label = Label(root, text='Порядок')
m_label.grid(row=2, column=1, ipadx=2, ipady=2, padx=5, pady=5)
m_entry = Entry(root)
m_entry.grid(row=2, column=2, ipadx=2, ipady=2, padx=5, pady=5)
m_entry.insert(0, '0.0')

dots_label = Label(root, text='Разрешение')
dots_label.grid(row=3, column=1, ipadx=2, ipady=2, padx=5, pady=5)
dots_entry = Entry(root)
dots_entry.grid(row=3, column=2, ipadx=2, ipady=2, padx=5, pady=5)
dots_entry.insert(0, '1024')

build_btn = Button(root, text='Построить', command=get_image)
build_btn.grid(row=1, column=3, rowspan=3, ipadx=5, ipady=6, padx=5, pady=5)

root.mainloop()
