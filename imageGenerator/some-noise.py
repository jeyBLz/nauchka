import os
from skimage.util import random_noise
import skimage
import skimage.io
import shutil
import re

path = 'C:\\Users\\andre\\OneDrive\\Рабочий стол\\Научка\\sources'

reg_exp = re.compile(r'noised')

dirs = os.listdir(path)

for curr_dir in dirs:
    curr_dir_path = path + '\\{}'.format(curr_dir)
    files = os.listdir(curr_dir_path)
    for curr_file in files:
        if reg_exp.search(curr_file):
            continue
        curr_file_path = curr_dir_path + '\\{}'.format(curr_file)

        new_file = random_noise(skimage.io.imread(curr_file_path), mode='gaussian', seed=None, clip=True)
        new_file_path = curr_dir_path + '\\{}{}'.format('noised', curr_file)
        skimage.io.imsave(new_file_path, new_file)

        new_file = random_noise(skimage.io.imread(curr_file_path), mode='pepper', seed=None, clip=True)
        new_file_path = curr_dir_path + '\\{}{}'.format('noised-p-', curr_file)
        skimage.io.imsave(new_file_path, new_file)

        new_file = random_noise(skimage.io.imread(curr_file_path), mode='speckle', seed=None, clip=True)
        new_file_path = curr_dir_path + '\\{}{}'.format('noised-spe-', curr_file)
        skimage.io.imsave(new_file_path, new_file)
