import os
import re

path = 'C:\\Users\\andre\\OneDrive\\Рабочий стол\\Научка\\training'

reg_exp = re.compile(r'noised-sp')

for curr_dir in os.listdir(path):
    curr_dir_path = path + '\\{}'.format(curr_dir)
    for curr_file in os.listdir(curr_dir_path):
        if reg_exp.search(curr_file):
            os.remove(curr_dir_path + '\\{}'.format(curr_file))
