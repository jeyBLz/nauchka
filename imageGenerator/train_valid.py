import os
import shutil

path = 'C:\\Users\\andre\\OneDrive\\Рабочий стол\\Научка\\training'

destination_path = 'C:\\Users\\andre\\OneDrive\\Рабочий стол\\Научка\\valid'

if not os.path.isdir(destination_path):
    os.mkdir(destination_path)

for curr_dir in os.listdir(path):
    curr_dir_path = path + '\\{}'.format(curr_dir)
    count = 0
    for curr_file in os.listdir(curr_dir_path):
        curr_file_path = curr_dir_path + '\\{}'.format(curr_file)
        destination_path_curr_dir = destination_path + '\\{}'.format(curr_dir)
        if not os.path.isdir(destination_path_curr_dir):
            os.mkdir(destination_path_curr_dir)
        if count % 10 == 0:
            shutil.move(curr_file_path, destination_path)
        count += 1
